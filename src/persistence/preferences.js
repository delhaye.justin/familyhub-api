const sql = require('sql-template-strings');
const {v4: uuidv4} = require('uuid');
const db = require('./db');

module.exports = {
  //Create a New Preference
  async create(family_id, family_name) {
    try {
      const {rows} = await db.query(sql`
      INSERT INTO preferences (id, family_id, family_name)
        VALUES (${uuidv4()}, ${family_id}, ${family_name})
        RETURNING id, family_id, family_name, updated_date;
      `);

      const [preferences] = rows;
      return preferences;
    } catch (error) {
      throw error;
    }
  },
  //Find a Family Preferences
  async find(family_id) {
    const {rows} = await db.query(sql`
    SELECT id, family_id, family_name, updated_date FROM preferences 
        WHERE family_id=${family_id} LIMIT 1;
    `);
    return rows[0];
  },
  //Change Family Name
  async setName(family_id, family_name) {
    const {rows} = await db.query(sql`
    UPDATE preferences SET family_name=${family_name}, updated_date=CURRENT_TIMESTAMP
        WHERE family_id=${family_id} 
        RETURNING id, family_id, family_name, updated_date;
    `);

    const [preferences] = rows;
    return preferences;
  }
};
