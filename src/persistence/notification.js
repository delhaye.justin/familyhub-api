const sql = require('sql-template-strings');
const {v4: uuidv4} = require('uuid');
const db = require('./db');

module.exports = {
  //Retrieve all Family Task
  async getAll(member_id) {
    const {rows} = await db.query(sql`
    SELECT type FROM notification
        WHERE view=${false} AND member_id=${member_id};
    `);
    return rows;
  },
  async viewAll(member_id) {
    const {rows} = await db.query(sql`
    UPDATE notification SET view=${true}
        WHERE view=${false} AND member_id=${member_id};
    `);

    const [notifications] = rows;
    return notifications;
  }
};
