const sql = require('sql-template-strings');
const {v4: uuidv4} = require('uuid');
const db = require('./db');

module.exports = {
  //Create a New Message
  async create(family_id, member_id, message) {
    try {
      const {rows} = await db.query(sql`
      INSERT INTO messages (id, family_id, member_id, message)
        VALUES (${uuidv4()}, ${family_id}, ${member_id}, ${message})
        RETURNING id, member_id, message, created_date;
      `);

      const [messages] = rows;
      return messages;
    } catch (error) {
      throw error;
    }
  },
  //Retrieve all Family Message
  async getAll(family_id) {
    const {rows} = await db.query(sql`
    SELECT m.message, m.member_id, g.username FROM messages m, members g
        WHERE m.family_id=${family_id} AND g.family_id=${family_id} AND m.member_id=g.id;
    `);
    return rows;
  }
};
