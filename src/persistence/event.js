const sql = require('sql-template-strings');
const {v4: uuidv4} = require('uuid');
const db = require('./db');

module.exports = {
  //Create a New Event
  async create(family_id, event_name, event_message, event_date) {
    try {
      const {rows} = await db.query(sql`
      INSERT INTO event (id, family_id, name, message, date)
        VALUES (${uuidv4()}, ${family_id}, ${event_name}, ${event_message}, ${event_date})
        RETURNING id, family_id, name, message, date, updated_date;
      `);

      const [event] = rows;
      return event;
    } catch (error) {
      throw error;
    }
  },
  //Retrieve all Family Events
  async getAll(family_id) {
    const {rows} = await db.query(sql`
    SELECT id, family_id, name, message, date, updated_date FROM event
        WHERE family_id=${family_id};
    `);
    return rows;
  },
  //Retrieve a Event
  async get(event_id) {
    const {rows} = await db.query(sql`
    SELECT id, family_id, name, message, date, updated_date FROM event
        WHERE id=${event_id};
    `);
    return rows;
  },
  //Change Event Message
  async changeMessage(event_id, message) {
    const {rows} = await db.query(sql`
    UPDATE event SET message=${message}, updated_date=CURRENT_TIMESTAMP
        WHERE id=${event_id} 
        RETURNING id, family_id, name, message, date, updated_date;
    `);

    const [event] = rows;
    return event;
  },
  //Change Event Name
  async changeName(event_id, name) {
    const {rows} = await db.query(sql`
    UPDATE event SET name=${name}, updated_date=CURRENT_TIMESTAMP
        WHERE id=${event_id} 
        RETURNING id, family_id, name, message, date, updated_date;
    `);

    const [event] = rows;
    return event;
  },
  //Change Event Name
  async changeDate(event_id, date) {
    const {rows} = await db.query(sql`
    UPDATE event SET date=${date}, updated_date=CURRENT_TIMESTAMP
        WHERE id=${event_id} 
        RETURNING id, family_id, name, message, date, updated_date;
    `);

    const [event] = rows;
    return event;
  },
  //Delete an Event 
  async delete(event_id) {
    await db.query(sql`
    DELETE FROM event WHERE id = ${event_id};
    `);
  }
};
