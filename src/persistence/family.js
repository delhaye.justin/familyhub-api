const sql = require('sql-template-strings');
const {v4: uuidv4} = require('uuid');
const bcrypt = require('bcrypt');
const db = require('./db');
const tools = require('../middleware/utils');

module.exports = {
  //Create a New Family
  async create(password) {
    try {
      const hashedPassword = await bcrypt.hash(password, 10);

      const {rows} = await db.query(sql`
      INSERT INTO families (id, password)
        VALUES (${uuidv4()}, ${hashedPassword})
        RETURNING id, created_date;
      `);

      const [family] = rows;
      return family;
    } catch (error) {
      throw error;
    }
  },
  async addRetrieveCode(family_id){
    const {rows} = await db.query(sql`
    UPDATE families SET retrieve_code=${tools.makecode()} 
      WHERE id=${family_id} RETURNING retrieve_code;
    `);
    return rows[0];
  },
  async findRetrieval(retrieve_code){
    const {rows} = await db.query(sql`
    SELECT * FROM families WHERE retrieve_code=${retrieve_code} LIMIT 1;
    `);
    return rows[0];
  },
  //Find a Family
  async find(family_id) {
    const {rows} = await db.query(sql`
    SELECT * FROM families WHERE id=${family_id} LIMIT 1;
    `);
    return rows[0];
  }
};
