const sql = require('sql-template-strings');
const {v4: uuidv4} = require('uuid');
const db = require('./db');
const tools = require('../middleware/utils');

module.exports = {
  //Create a New Member
  async create(family_id, creator, role_id, username) {
    try {
      const {rows} = await db.query(sql`
      INSERT INTO members (id, family_id, role_id, creator, username, invite_code)
        VALUES (${uuidv4()}, ${family_id}, ${role_id}, ${creator}, ${username}, ${tools.makeid()})
        RETURNING id, family_id, creator, username, invite_code;
      `);

      const [members] = rows;
      return members;
    } catch (error) {
      throw error;
    }
  },
  //Find a Family Member
  async find(member_id) {
    const {rows} = await db.query(sql`
    SELECT m.id, m.family_id, m.creator, m.role_id, m.username, m.invite_code, m.connection_token, r.name FROM members m, roles r
        WHERE m.id=${member_id} AND r.id = m.role_id LIMIT 1;
    `);
    return rows[0];
  },
  //Create a Family Member Token
  async loadToken(member_id) {
    const {rows} = await db.query(sql`
    UPDATE members SET connection_token=${tools.maketoken()}
        WHERE id=${member_id} RETURNING connection_token;
    `);
    return rows[0];
  },
  //Find a Family Member
  async findCode(invite_code) {
    const {rows} = await db.query(sql`
    SELECT m.id, m.family_id, m.creator, m.role_id, m.username, m.invite_code, r.name FROM members m, roles r
        WHERE m.invite_code=${invite_code} AND r.id = m.role_id LIMIT 1;
    `);
    return rows[0];
  },
  //Find a Family Member
  async findCreator(family_id) {
    const {rows} = await db.query(sql`
    SELECT * FROM members
        WHERE family_id=${family_id} AND creator = TRUE LIMIT 1;
    `);
    return rows[0];
  },
  //Find a Family Member
  async findToken(connection_token) {
    const {rows} = await db.query(sql`
    SELECT m.id, m.family_id, m.creator, m.role_id, m.username, m.invite_code, r.name FROM members m, roles r
        WHERE m.connection_token=${connection_token} AND r.id = m.role_id LIMIT 1;
    `);
    return rows[0];
  },
  //Retrieve all Family Members
  async getAll(family_id) {
    const {rows} = await db.query(sql`
    SELECT m.id, m.family_id, m.creator, m.role_id, m.username, m.invite_code, r.name FROM members m, roles r
        WHERE m.family_id=${family_id} AND r.id = m.role_id;
    `);
    return rows;
  },
  //Retrieve all Family Members
  async changeRole(member_id, role_id) {
    const {rows} = await db.query(sql`
    UPDATE members SET role_id=${role_id}
        WHERE id=${member_id};
    `);
    return rows[0];
  },
  //Delete a Family Member
  async delete(member_id) {
    await db.query(sql`
    DELETE FROM members WHERE id = ${member_id};
    `);
  }
};
