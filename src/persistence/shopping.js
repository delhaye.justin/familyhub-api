const sql = require('sql-template-strings');
const {v4: uuidv4} = require('uuid');
const db = require('./db');

module.exports = {
  //Create a New Message
  async create(family_id, list_name) {
    try {
      const {rows} = await db.query(sql`
      INSERT INTO shopping (id, family_id, name)
        VALUES (${uuidv4()}, ${family_id}, ${list_name})
        RETURNING id, family_id, name, items, updated_date;
      `);

      const [shopping] = rows;
      return shopping;
    } catch (error) {
      throw error;
    }
  },
  //Retrieve all Family Shopping List
  async getAll(family_id) {
    const {rows} = await db.query(sql`
    SELECT id, family_id, name, items, updated_date FROM shopping
        WHERE family_id=${family_id};
    `);
    return rows;
  },
  //Retrieve a Shopping List
  async get(list_id) {
    const {rows} = await db.query(sql`
    SELECT id, family_id, name, items, updated_date FROM shopping
        WHERE id=${list_id};
    `);
    return rows;
  },
  //Change Shopping List Name
  async changeName(list_id, list_name){
    const {rows} = await db.query(sql`
    UPDATE shopping SET name=${list_name}, updated_date=CURRENT_TIMESTAMP
        WHERE id=${list_id} 
        RETURNING id, family_id, name, items, updated_date;
    `);

    const [shopping] = rows;
    return shopping;
  },
  //Change Shopping List Items
  async changeItems(list_id, items){
    const {rows} = await db.query(sql`
    UPDATE shopping SET items=${items}, updated_date=CURRENT_TIMESTAMP
        WHERE id=${list_id} 
        RETURNING id, family_id, name, items, updated_date;
    `);

    const [shopping] = rows;
    return shopping;
  },
  //Delete a Shopping List
  async delete(list_id) {
    await db.query(sql`
    DELETE FROM shopping WHERE id = ${list_id};
    `);
  }
};
