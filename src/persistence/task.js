const sql = require('sql-template-strings');
const {v4: uuidv4} = require('uuid');
const db = require('./db');

module.exports = {
  //Create a New Task
  async create(family_id, task_description, date, member_id) {
    try {
      const {rows} = await db.query(sql`
      INSERT INTO task (id, family_id, description, todo_date, member_id)
        VALUES (${uuidv4()}, ${family_id}, ${task_description}, ${date}, ${member_id})
        RETURNING id, family_id, description, todo_date, member_id, updated_date;
      `);

      await db.query(sql`
      INSERT INTO notification (id, member_id, type)
        VALUES (${uuidv4()}, ${member_id}, ${"Task"});
      `);

      const [task] = rows;
      return task;
    } catch (error) {
      throw error;
    }
  },
  //Retrieve all Family Task
  async getAll(family_id) {
    const {rows} = await db.query(sql`
    SELECT t.id, t.family_id, t.description, t.todo_date, t.done_date, t.member_id, t.updated_date, m.username FROM task t, members m
        WHERE t.family_id=${family_id} AND m.family_id=${family_id} AND m.id=t.member_id;
    `);
    return rows;
  },
  //Retrieve all Family Task for a member
  async getAllMember(family_id, member_id) {
    const {rows} = await db.query(sql`
    SELECT id, family_id, description, todo_date, done_date, member_id, updated_date FROM task
        WHERE family_id=${family_id} AND member_id=${member_id};
    `);
    return rows;
  },
  //Set a task to done
  async setTaskDone(task_id, date) {
    const {rows} = await db.query(sql`
    UPDATE task SET done_date=${date}
        WHERE id=${task_id};
    `);
    return rows;
  },
  //Set a task to done
  async setTaskDescription(task_id, description) {
    const {rows} = await db.query(sql`
    UPDATE task SET description=${description}
        WHERE id=${task_id};
    `);
    return rows;
  },
  //Retrieve a Task
  async get(task_id) {
    const {rows} = await db.query(sql`
    SELECT id, family_id, description, todo_date, done_date, member_id, updated_date FROM task
        WHERE id=${task_id};
    `);
    return rows;
  },
  //Delete a Task
  async delete(task_id) {
    await db.query(sql`
    DELETE FROM task WHERE id = ${task_id};
    `);
  }
};
