const sql = require('sql-template-strings');
const db = require('./db');

module.exports = {
  //Find a Role by name
  async find(role_name) {
    const {rows} = await db.query(sql`
    SELECT * FROM roles WHERE name=${role_name} LIMIT 1;
    `);
    return rows[0];
  },
  //Find a Role by id
  async findId(role_id) {
    const {rows} = await db.query(sql`
    SELECT * FROM roles WHERE id=${role_id} LIMIT 1;
    `);
    return rows[0];
  },
  //Get all Roles
  async get() {
    const {rows} = await db.query(sql`
    SELECT * FROM roles;
    `);
    return rows;
  }
};
