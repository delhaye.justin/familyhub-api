const sql = require('sql-template-strings');
const {v4: uuidv4} = require('uuid');
const db = require('./db');

module.exports = {
  //Initiate New Session
  async create(member_id) {
    const id = uuidv4();
    await db.query(sql`
    INSERT INTO sessions (id, member_id)
      VALUES (${id}, ${member_id});
    `);
    return id;
  },
  //Find Session
  async find(id) {
    const {rows} = await db.query(sql`
    SELECT member_id FROM sessions WHERE id = ${id} LIMIT 1;
    `);
    if (rows.length !== 1) {
      return null;
    }

    const {member_id: member_id} = rows[0];
    return {member_id};
  },
  //Delete Session
  async delete(id) {
    await db.query(sql`
    DELETE FROM sessions WHERE id = ${id};
    `);
  }
};
