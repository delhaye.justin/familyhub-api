module.exports = {
  makeid: function () {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  
    for (var i = 0; i < 10; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  },
  maketoken: function() {
    var text = "";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  
    for (var i = 0; i < 11; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    text += "-"
  
    for (var i = 0; i < 11; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  },
  makecode: function() {
    var text = "RET";
    var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789";
  
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    text += "-"
  
    for (var i = 0; i < 5; i++)
      text += possible.charAt(Math.floor(Math.random() * possible.length));
  
    return text;
  }
}