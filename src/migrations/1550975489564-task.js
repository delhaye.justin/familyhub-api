const db = require('../persistence/db');

module.exports.up = async function (next) {
  const client = await db.connect();

  await client.query(`
  CREATE TABLE IF NOT EXISTS task (
    id uuid NOT NULL UNIQUE,
    family_id uuid NOT NULL REFERENCES families (id) ON DELETE CASCADE,
    description text NOT NULL,
    member_id uuid NOT NULL REFERENCES members (id) ON DELETE CASCADE,
    todo_date TIMESTAMP NOT NULL,
    done_date TIMESTAMP,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP,
    deleted_date TIMESTAMP,
    PRIMARY KEY(id, family_id)
  );
  `);

  await client.release(true);
  next();
};

module.exports.down = async function (next) {
  const client = await db.connect();

  await client.query(`
  DROP TABLE task;
  `);

  await client.release(true);
  next();
};
