const db = require('../persistence/db');

module.exports.up = async function (next) {
  const client = await db.connect();

  await client.query(`
  CREATE TABLE IF NOT EXISTS notification (
    id uuid NOT NULL UNIQUE,
    member_id uuid NOT NULL REFERENCES members (id) ON DELETE CASCADE,
    type TEXT NOT NULL,
    view BOOLEAN DEFAULT FALSE,
    PRIMARY KEY(id, member_id)
  );
  `);

  await client.release(true);
  next();
};

module.exports.down = async function (next) {
  const client = await db.connect();

  await client.query(`
  DROP TABLE notification;
  `);

  await client.release(true);
  next();
};
