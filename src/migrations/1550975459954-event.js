const db = require('../persistence/db');

module.exports.up = async function (next) {
  const client = await db.connect();

  await client.query(`
  CREATE TABLE IF NOT EXISTS event (
    id uuid NOT NULL UNIQUE,
    family_id uuid NOT NULL REFERENCES families (id) ON DELETE CASCADE,
    name text NOT NULL,
    message text NOT NULL,
    date TIMESTAMP NOT NULL,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP,
    deleted_date TIMESTAMP,
    PRIMARY KEY(id, family_id)
  );
  `);

  await client.release(true);
  next();
};

module.exports.down = async function (next) {
  const client = await db.connect();

  await client.query(`
  DROP TABLE event;
  `);

  await client.release(true);
  next();
};
