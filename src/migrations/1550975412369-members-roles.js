const db = require('../persistence/db');
const {v4: uuidv4} = require('uuid');

module.exports.up = async function (next) {
  const client = await db.connect();

  await client.query(`
  CREATE TABLE IF NOT EXISTS roles (
    id uuid PRIMARY KEY,
    name text UNIQUE NOT NULL
  );
  `);

  await client.query(`
  INSERT INTO roles (id, name) VALUES 
    ('${uuidv4()}', 'Parent'),
    ('${uuidv4()}', 'Enfant');
  `)

  await client.query(`
  CREATE TABLE IF NOT EXISTS members (
    id uuid NOT NULL UNIQUE,
    family_id uuid NOT NULL REFERENCES families (id) ON DELETE CASCADE,
    role_id uuid NOT NULL REFERENCES roles (id),
    creator boolean NOT NULL DEFAULT FALSE,
    username text NOT NULL,
    invite_code text,
    connection_token text,
    created_date TIMESTAMP DEFAULT CURRENT_TIMESTAMP,
    updated_date TIMESTAMP,
    deleted_date TIMESTAMP,
    PRIMARY KEY(id, family_id)
  );

  CREATE TABLE IF NOT EXISTS sessions (
    id uuid PRIMARY KEY,
    member_id uuid REFERENCES members (id) ON DELETE CASCADE
  );
  `);

  await client.query(`
  CREATE INDEX sessions_member on sessions (member_id);
  `);

  await client.release(true);
  next();
};

module.exports.down = async function (next) {
  const client = await db.connect();

  await client.query(`
  DROP TABLE sessions;
  DROP TABLE members;
  DROP TABLE roles;
  `);

  await client.release(true);
  next();
};
