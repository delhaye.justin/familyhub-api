const db = require('../persistence/db');

module.exports.up = async function (next) {
  const client = await db.connect();

  await client.query(`
  CREATE TABLE IF NOT EXISTS preferences (
    id uuid NOT NULL,
    family_id uuid NOT NULL REFERENCES families (id) ON DELETE CASCADE,
    family_name text NOT NULL,
    updated_date TIMESTAMP,
    PRIMARY KEY(id, family_id)
  );
  `);

  await client.release(true);
  next();
};

module.exports.down = async function (next) {
  const client = await db.connect();

  await client.query(`
  DROP TABLE preferences;
  `);

  await client.release(true);
  next();
};
