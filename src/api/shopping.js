const {Router} = require('express');

const Member = require('../persistence/member');
const Role = require('../persistence/roles');
const Session = require('../persistence/sessions');
const Shopping = require('../persistence/shopping.js');

const sessionMiddleware = require('../middleware/session-middleware');

const router = new Router();

//Post to Create a New Shopping List
router.post('/', sessionMiddleware, async (request, response) => {
    try {
        const {list_name} = request.body;
        const session = await Session.find(request.session.id)

        if (!list_name) {
            return response.status(400).json({message: "List name must be provided"});
        }

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const shopping_list = await Shopping.create(member.family_id, list_name);

        return response.status(200).json(shopping_list);
    } catch (error) {
      console.error(
        `createShoppingList({ id: ${request.session.id} }) >> Error: ${error.stack}`
      );
      response.status(500).json();
    }
  });

//Get to Retrieve Family Shopping List
router.get('/', sessionMiddleware, async (request, response) => {
    try {
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const shopping_list = await Shopping.getAll(member.family_id);

        return response.status(200).json(shopping_list);
    } catch (error) {
        console.error(
            `getShoppingList({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

//Get to Retrieve Family Shopping List
router.put('/:id', sessionMiddleware, async (request, response) => {
    try {
        const list_id = request.params.id;
        const {list_name, items} = request.body;
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const shopping_list = await Shopping.get(list_id);
        if (!shopping_list && shopping_list.family_id == member.family_id) {
            return response.status(403).json({message: "Unknown List or Not Your Family List"});
        }

        if(list_name){
            await Shopping.changeName(list_id, list_name);
        }

        if(items){
            await Shopping.changeItems(list_id, items);
        }

        const shopping_lists = await Shopping.get(list_id);

        return response.status(200).json(shopping_lists);
    } catch (error) {
        console.error(
            `getShoppingList({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

//Get to Retrieve Family Shopping List
router.delete('/:id', sessionMiddleware, async (request, response) => {
    try {
        const list_id = request.params.id;
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const shopping_list = await Shopping.get(list_id);
        if (!shopping_list && shopping_list.family_id == member.family_id) {
            return response.status(403).json({message: "Unknown List or Not Your Family List"});
        }

        const role = await Role.findId(member.role_id);
        if(!role || role.name != "Parent") {
            return response.status(403).json({message: "Unknown Role Or Wrong Authorization"});
        }

        await Shopping.delete(list_id);

        const shopping_lists = await Shopping.getAll(member.family_id);

        return response.status(200).json(shopping_lists);
    } catch (error) {
        console.error(
            `deleteShoppingList({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

module.exports = router;
