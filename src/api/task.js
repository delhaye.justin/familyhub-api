const {Router} = require('express');

const Member = require('../persistence/member');
const Role = require('../persistence/roles');
const Session = require('../persistence/sessions');
const Task = require('../persistence/task.js');

const sessionMiddleware = require('../middleware/session-middleware');

const router = new Router();

//Post to Create a New Task
router.post('/', sessionMiddleware, async (request, response) => {
    try {
        const {description, date, member_id} = request.body;
        const session = await Session.find(request.session.id)

        if (!description || !date ||!member_id) {
            return response.status(400).json({message: "Description, date and member id must be provided"});
        }

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const todo_member = await Member.find(member_id);
        if (!todo_member) {
            return response.status(403).json({message: "Unknown Member"});
        }

        const task = await Task.create(member.family_id, description, date, todo_member.id);

        return response.status(200).json(task);
    } catch (error) {
      console.error(
        `createTask({ id: ${request.session.id} }) >> Error: ${error.stack}`
      );
      response.status(500).json();
    }
  });

//Get to Retrieve Family Tasks
router.get('/', sessionMiddleware, async (request, response) => {
    try {
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const tasks = await Task.getAll(member.family_id);

        return response.status(200).json(tasks);
    } catch (error) {
        console.error(
            `getTaskList({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

//Get to set Task to done
router.put('/:id', sessionMiddleware, async (request, response) => {
    try {
        const task_id = request.params.id;
        const {done_date, description} = request.body;
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const task = await Task.get(task_id);
        if(!task) {
            return response.status(403).json({message: "Unknow Task"});
        }

        if(done_date) {
            await Task.setTaskDone(task_id, done_date);
        }

        if(description) {
            await Task.setTaskDescription(task_id, description);
        }

        const tasks = await Task.get(task_id);

        return response.status(200).json(tasks);
    } catch (error) {
        console.error(
            `setTaskDone({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

//Get to Retrieve Family Tasks
router.get('/:id', sessionMiddleware, async (request, response) => {
    try {
        const task_id = request.params.id;
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const task = await Task.get(task_id);

        return response.status(200).json(task);
    } catch (error) {
        console.error(
            `getTaskList({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

//Get to Delete Family Task
router.delete('/:id', sessionMiddleware, async (request, response) => {
    try {
        const task_id = request.params.id;
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const task = await Task.get(task_id);
        if (!task && task.family_id == member.family_id) {
            return response.status(403).json({message: "Unknown Task or Not Your Family Task"});
        }

        const role = await Role.findId(member.role_id);
        if(!role || role.name != "Parent") {
            return response.status(403).json({message: "Unknown Role Or Wrong Authorization"});
        }

        await Task.delete(task_id);

        const tasks = await Task.getAll(member.family_id);

        return response.status(200).json(tasks);
    } catch (error) {
        console.error(
            `deleteTask({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

module.exports = router;
