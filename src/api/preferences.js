const {Router} = require('express');
const bcrypt = require('bcrypt');

const Preferences = require('../persistence/preferences');
const Family = require('../persistence/family');
const Session = require('../persistence/sessions');
const Member = require('../persistence/member');
const Role = require('../persistence/roles');

const sessionMiddleware = require('../middleware/session-middleware');

const router = new Router();

//Get to Retrieve Family Preferences
router.get('/', sessionMiddleware, async (request, response) => {
    try {
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const preferences = await Preferences.find(member.family_id);

        return response.status(200).json(preferences);
    } catch (error) {
        console.error(
            `getPreference({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

//Put to Modify Preferences
router.put('/', sessionMiddleware, async (request, response) => {
    try {
        const {password, family_name} = request.body;
        const session = await Session.find(request.session.id)
        
        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const role = await Role.findId(member.role_id);
        if(!role || role.name != "Parent") {
            return response.status(403).json({message: "Unknown Role Or Wrong Authorization"});
        }

        const family = await Family.find(member.family_id);
        if (!family || !(await bcrypt.compare(password, family.password))) {
            return response.status(403).json({message: "Incorrect Password or Unknown Family"});
        }

        if(family_name){
            const updated_family = await Preferences.setName(member.family_id, family_name);
            return response.status(200).json(updated_family);
        }

        const preferences = await Preferences.find(member.family_id);

        return response.status(200).json(preferences);
    } catch (error) {
        console.error(
            `putPreference({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

module.exports = router;
