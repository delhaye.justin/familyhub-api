const {Router} = require('express');
const bcrypt = require('bcrypt');

const Members = require('../persistence/member');
const Family = require('../persistence/family');
const Session = require('../persistence/sessions');

const sessionMiddleware = require('../middleware/session-middleware');

const router = new Router();

//Post to Create a New Session
router.post('/', async (request, response) => {
  try {
    const {invite_code, password} = request.body;

    if (!invite_code || !password) {
        return response.status(400).json({message: 'Invite Code and Password must be provided'});
    }

    const member = await Members.findCode(invite_code);
    if (!member || !invite_code) {
      return response.status(403).json({message: "Wrong Invite Code"});
    }

    const family = await Family.find(member.family_id);
    if (!family || !(await bcrypt.compare(password, family.password))) {
        return response.status(403).json({message: "Incorrect Password or Unknown Family !"});
    }

    const sessionId = await Session.create(member.id);

    request.session.id = sessionId;
    response.status(201).json(member);
  } catch (error) {
    console.error(
      `POST session ({ id: ${request.body.id} }) >> ${error.stack})`
    );
    response.status(500).json();
  }
});

//Post to Create a New Session
router.post('/:token', async (request, response) => {
  try {
    const connection_token = request.params.token;

    if (!connection_token) {
        return response.status(400).json({message: 'Connection Token must be provided'});
    }

    const member = await Members.findToken(connection_token);
    if (!member) {
      return response.status(403).json({message: "Wrong Token"});
    }

    const sessionId = await Session.create(member.id);

    request.session.id = sessionId;
    response.status(201).json(member);
  } catch (error) {
    console.error(
      `POST session ({ id: ${request.body.id} }) >> ${error.stack})`
    );
    response.status(500).json();
  }
});

//Post to Create a New Session
router.post('/retrieve/:retrieve_code', async (request, response) => {
  try {
    const {password} = request.body;
    const retrieve_code = request.params.retrieve_code;

    if (!retrieve_code) {
        return response.status(400).json({message: 'Retrieve Code must be provided'});
    }

    const family = await Family.findRetrieval(retrieve_code);
    if (!family || !(await bcrypt.compare(password, family.password))) {
      return response.status(403).json({message: "Incorrect Password or Unknown Family !"});
    }

    const member = await Members.findCreator(family.id);
    if (!member) {
      return response.status(403).json({message: "Unknown Member"});
    }

    const sessionId = await Session.create(member.id);

    request.session.id = sessionId;
    response.status(201).json(member);
  } catch (error) {
    console.error(
      `POST session ({ id: ${request.body.id} }) >> ${error.stack})`
    );
    response.status(500).json();
  }
});

//Get to Find User Session
router.get('/', sessionMiddleware, (request, response) => {
  response.json({member_id: request.member_id});
});

//Delete to Close User Session
router.delete('/', async (request, response) => {
  try {
    if (request.session.id) {
      await Session.delete(request.session.id);
    }

    request.session.id = null;
    response.status(200).json();
  } catch (error) {
    console.error(`DELETE session >> ${error.stack}`);
    response.status(500).json();
  }
});

module.exports = router;
