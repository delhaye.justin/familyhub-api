const {Router} = require('express');

const Member = require('../persistence/member');
const Role = require('../persistence/roles');
const Session = require('../persistence/sessions');
const Event = require('../persistence/event.js');

const sessionMiddleware = require('../middleware/session-middleware');

const router = new Router();

//Post to Create a New Event
router.post('/', sessionMiddleware, async (request, response) => {
    try {
        const {name, message, date} = request.body;
        const session = await Session.find(request.session.id)

        if (!name || !message || !date) {
            return response.status(400).json({message: "List name, message and date must be provided"});
        }

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const role = await Role.findId(member.role_id);
        if(!role || role.name != "Parent") {
            return response.status(403).json({message: "Unknown Role Or Wrong Authorization"});
        }

        const event = await Event.create(member.family_id, name, message, date);

        return response.status(200).json(event);
    } catch (error) {
      console.error(
        `createEvent({ id: ${request.session.id} }) >> Error: ${error.stack}`
      );
      response.status(500).json();
    }
  });

//Get to Retrieve Family Events
router.get('/', sessionMiddleware, async (request, response) => {
    try {
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const events= await Event.getAll(member.family_id);

        return response.status(200).json(events);
    } catch (error) {
        console.error(
            `getEvents({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

//Put to Change Event Data
router.put('/:id', sessionMiddleware, async (request, response) => {
    try {
        const event_id = request.params.id;
        const {name, message, date} = request.body;
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const role = await Role.findId(member.role_id);
        if(!role || role.name != "Parent") {
            return response.status(403).json({message: "Unknown Role Or Wrong Authorization"});
        }

        const event = await Event.get(event_id);
        if (!event && event.family_id == member.family_id) {
            return response.status(403).json({message: "Unknown Event or Not Your Family List"});
        }

        if(name){
            await Event.changeName(event_id, name);
        }

        if(message){
            await Event.changeMessage(event_id, message);
        }

        if(date){
            await Event.changeDate(event_id, date);
        }

        const events = await Event.get(event_id);

        return response.status(200).json(events);
    } catch (error) {
        console.error(
            `getEvent({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

//Delete to Remove Event Data
router.delete('/:id', sessionMiddleware, async (request, response) => {
    try {
        const event_id = request.params.id;
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const event = await Event.get(event_id);
        if (!event && event.family_id == member.family_id) {
            return response.status(403).json({message: "Unknown Event or Not Your Family List"});
        }

        const role = await Role.findId(member.role_id);
        if(!role || role.name != "Parent") {
            return response.status(403).json({message: "Unknown Role Or Wrong Authorization"});
        }

        await Event.delete(event_id);

        const events = await Event.getAll(member.family_id);

        return response.status(200).json(events);
    } catch (error) {
        console.error(
            `deleteEvent({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

module.exports = router;