const express = require('express');

const {Router} = express;
const router = new Router();

const family = require('./family');
const session = require('./session');
const preferences = require('./preferences');
const members = require('./member');
const roles = require('./role');
const messages = require('./message');
const shopping = require('./shopping');
const event = require('./event');
const task = require('./task');
const notification = require('./notification');

router.use('/api/families', family);
router.use('/api/sessions', session);
router.use('/api/preferences', preferences);
router.use('/api/members', members);
router.use('/api/roles', roles);
router.use('/api/messages', messages);
router.use('/api/shopping', shopping);
router.use('/api/events', event);
router.use('/api/tasks', task);
router.use('/api/notification', notification);

module.exports = router;
