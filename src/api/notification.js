const {Router} = require('express');

const Member = require('../persistence/member');
const Notification = require('../persistence/notification.js');

const router = new Router();

//Get to Retrieve User Notifications
router.get('/:id', async (request, response) => {
    try {
        const member_id = request.params.id;

        const member = await Member.find(member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const notifications = await Notification.getAll(member.id);
        await Notification.viewAll(member.id);

        return response.status(200).json(notifications);
    } catch (error) {
        console.error(
            `getTaskList({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

module.exports = router;
