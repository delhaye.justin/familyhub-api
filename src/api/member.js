const {Router} = require('express');

const Member = require('../persistence/member');
const Role = require('../persistence/roles');
const Session = require('../persistence/sessions');

const sessionMiddleware = require('../middleware/session-middleware');

const router = new Router();

//Post to Create a New Family Member
router.post('/', sessionMiddleware, async (request, response) => {
    try {
        const {username, role_name} = request.body;
        if (!username || !role_name) {
            return response.status(400).json({message: 'Username and Role must be provided'});
        }

        const session = await Session.find(request.session.id);
        if (!session) {
            return response.status(403).json({message: "Unknown Session"});
        }
  
        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Unknown Member"});
        }

        const post_role = await Role.findId(member.role_id);
        if(!post_role || post_role.name != "Parent") {
            return response.status(403).json({message: "Unknown Role Or Wrong Authorization"});
        }

        const role = await Role.find(role_name);
        if(!role) {
            return response.status(403).json({message: "Unknown Role"});
        }

        await Member.create(member.family_id, false, role.id, username);

        const members = await Member.getAll(member.family_id);

        return response.status(200).json(members);
    } catch (error) {
      console.error(
        `createMember({ username: ${request.body.username} }) >> Error: ${error.stack}`
      );
      response.status(500).json();
    }
});

//Put to change user role
router.put('/:id/parent', sessionMiddleware, async (request, response) => {
    try {
        const user_id = request.params.id;

        const session = await Session.find(request.session.id);
        if (!session) {
            return response.status(403).json({message: "Unknown Session"});
        }
  
        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Unknown Member"});
        }

        const post_role = await Role.findId(member.role_id);
        if(!post_role || post_role.name != "Parent") {
            return response.status(403).json({message: "Unknown Role Or Wrong Authorization"});
        }

        const new_role = await Role.find("Parent");
        if(!new_role) {
            return response.status(403).json({message: "Unknown Role"});
        }

        const members = await Member.changeRole(user_id, new_role.id)

        return response.status(200).json(members);
    } catch (error) {
      console.error(
        `setMemberRole({ id: ${request.session.id} }) >> Error: ${error.stack}`
      );
      response.status(500).json();
    }
});

//Put to change user role
router.put('/:id/child', sessionMiddleware, async (request, response) => {
    try {
        const user_id = request.params.id;

        const session = await Session.find(request.session.id);
        if (!session) {
            return response.status(403).json({message: "Unknown Session"});
        }
  
        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Unknown Member"});
        }

        const post_role = await Role.findId(member.role_id);
        if(!post_role || post_role.name != "Parent") {
            return response.status(403).json({message: "Unknown Role Or Wrong Authorization"});
        }

        const new_role = await Role.find("Enfant");
        if(!new_role) {
            return response.status(403).json({message: "Unknown Role"});
        }

        const members = await Member.changeRole(user_id, new_role.id)

        return response.status(200).json(members);
    } catch (error) {
      console.error(
        `setMemberRole({ id: ${request.session.id} }) >> Error: ${error.stack}`
      );
      response.status(500).json();
    }
});

//Post to add user token
router.put('/:id/token', sessionMiddleware, async (request, response) => {
    try {
        const user_id = request.params.id;

        const session = await Session.find(request.session.id);
        if (!session) {
            return response.status(403).json({message: "Unknown Session"});
        }
  
        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Unknown Member"});
        }

        const post_role = await Role.findId(member.role_id);
        if(!post_role || post_role.name != "Parent") {
            return response.status(403).json({message: "Unknown Role Or Wrong Authorization"});
        }

        const token = await Member.loadToken(user_id);

        return response.status(200).json(token);
    } catch (error) {
      console.error(
        `createMemberToken({ id: ${request.session.id} }) >> Error: ${error.stack}`
      );
      response.status(500).json();
    }
});

//Delete to Remove a New Family Member
router.delete('/:id', sessionMiddleware, async (request, response) => {
    try {
        const member_id = request.params.id;
        if (!member_id) {
            return response.status(400).json({message: 'member id must be provided'});
        }

        const session = await Session.find(request.session.id);
        if (!session) {
            return response.status(403).json("Unknown Session");
        }
  
        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json("Unknown Member");
        }

        const role = await Role.findId(member.role_id);
        if(!role || role.name != "Parent") {
            return response.status(403).json("Unknown Role Or Wrong Authorization");
        }

        await Member.delete(member_id);

        const members = await Member.getAll(member.family_id);
        if(!members) {
            return response.status(403).json("No Members in your family");
        }

        return response.status(200).json(members);
    } catch (error) {
      console.error(
        `removeMember({ username: ${request.body.member_id} }) >> Error: ${error.stack}`
      );
      response.status(500).json();
    }
});

//Get to Retrieve Family Members
router.get('/', sessionMiddleware, async (request, response) => {
    try {
        const session = await Session.find(request.session.id);
        if (!session) {
            return response.status(403).json("Unknown Session");
        }

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json("Incorrect Member");
        }

        const members = await Member.getAll(member.family_id);
        if(!members) {
            return response.status(403).json("No Members in your family");
        }

        return response.status(200).json(members);
    } catch (error) {
        console.error(
            `getMembers({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

module.exports = router;
