const {Router} = require('express');

const Family = require('../persistence/family');
const Preferences = require('../persistence/preferences');
const Members = require('../persistence/member');
const Role = require('../persistence/roles');
const Session = require('../persistence/sessions');

const router = new Router();

//Post to Create a New Family
router.post('/', async (request, response) => {
  try {
    const {name, username, password} = request.body;
    if (!name || !username || !password) {
      return response.status(400).json({message: 'Family name, Username and Password must be provided'});
    }

    const role = await Role.find('Parent');
    if(!role) {
        return response.status(403).json({message: "Unknown Role"});
    }

    let strongPassword = new RegExp('(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.{8,})')
    if(!strongPassword.test(password)){
        return response.status(403).json({message: "Password Strength Error"});
    }

    const family = await Family.create(password);
    const preferences = await Preferences.create(family.id, name);
    const members = await Members.create(family.id, true, role.id, username);

    return response.status(200).json({family: family, preferences: preferences, creator: members});
  } catch (error) {
    console.error(
      `createFamily({ name: ${request.body.name} }) >> Error: ${error.stack}`
    );
    response.status(500).json();
  }
});

//Get to create a new family retrieve code
router.get('/retrieve', async (request, response) => {
  try {
    const session = await Session.find(request.session.id)

    const member = await Members.find(session.member_id);
    if (!member || !member.creator) {
        return response.status(403).json({message: "Incorrect Member or Not the family creator"});
    }

    const code = await Family.addRetrieveCode(member.family_id)

    return response.status(200).json(code);
  } catch (error) {
    console.error(
      `createFamily({ name: ${request.body.name} }) >> Error: ${error.stack}`
    );
    response.status(500).json();
  }
});

module.exports = router;
