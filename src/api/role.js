const {Router} = require('express');

const Role = require('../persistence/roles');

const router = new Router();

//Get this list of all roles
router.get('/', async (request, response) => {
    try {
        const role = await Role.get();
        if(!role) {
            return response.status(403).json({message: "Can't find Roles"});
        }

        return response.status(200).json(role);
    } catch (error) {
        console.error(
            `getRole({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

module.exports = router;
