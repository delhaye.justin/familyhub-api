const {Router} = require('express');

const Member = require('../persistence/member');
const Message = require('../persistence/message');
const Session = require('../persistence/sessions');

const sessionMiddleware = require('../middleware/session-middleware');

const router = new Router();

//Post to Post a New Message
router.post('/', sessionMiddleware, async (request, response) => {
    try {
        const {message} = request.body;
        const session = await Session.find(request.session.id)

        if (!message) {
            return response.status(400).json({message: 'A Message must be provided'});
        }

        const member = await Member.find(session.member_id);
        if(!member) {
            return response.status(403).json({message: "Unknown Member"});
        }

        const messages = await Message.create(member.family_id, member.id, message);

        return response.status(200).json(messages);
    } catch (error) {
      console.error(
        `createMessage({ member: ${request.body.member_id} }) >> Error: ${error.stack}`
      );
      response.status(500).json();
    }
  });

//Get to Retrieve Family Message
router.get('/', sessionMiddleware, async (request, response) => {
    try {
        const session = await Session.find(request.session.id)

        const member = await Member.find(session.member_id);
        if (!member) {
            return response.status(403).json({message: "Incorrect Member"});
        }

        const messages = await Message.getAll(member.family_id);

        return response.status(200).json(messages);
    } catch (error) {
        console.error(
            `getMessages({ id: ${request.session.id} }) >> Error: ${error.stack}`
        );
        response.status(500).json();
    }
});

module.exports = router;
